package com.andrej.rogueminer.desktop

import com.andrej.rogueminer.Rogueminer
import com.badlogic.gdx.backends.lwjgl.*

fun main(arg: Array<String>) {
    val config = LwjglApplicationConfiguration()
    config.width = 800
    config.height = 600
    config.resizable = false
    LwjglApplication(Rogueminer, config)
}
