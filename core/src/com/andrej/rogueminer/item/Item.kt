package com.andrej.rogueminer.item

import com.andrej.rogueminer.util.Assets
import com.badlogic.gdx.graphics.Texture
import java.math.BigInteger
import java.util.*


enum class Item(val itemName: String,
                val price: BigInteger,
                val order: Int,
                val texture: Texture? = Assets.Textures.Items.debugTexture,
                val recipe: Recipe? = null)
{
    // Non-metal resources
    ICE("water ice", BigInteger.valueOf(10), 1, Assets.Textures.Items.ice),

    // Native metals
    TIN("tin", BigInteger.valueOf(3), 101, Assets.Textures.Items.tin),
    COPPER("copper", BigInteger.valueOf(4), 102, Assets.Textures.Items.copper),
    IRON("iron", BigInteger.valueOf(5), 103, Assets.Textures.Items.iron),
    SILVER("silver", BigInteger.valueOf(10), 104, Assets.Textures.Items.silver),
    GOLD("gold", BigInteger.valueOf(15), 105, Assets.Textures.Items.gold),
    LEAD("lead", BigInteger.valueOf(3), 106, Assets.Textures.Items.lead),
    ALUMINIUM("aluminum", BigInteger.valueOf(5), 107),
    TITANIUM("titanium", BigInteger.valueOf(8), 108),
    TUNGSTEN("tungsten", BigInteger.valueOf(9), 109),
    OSMIUM("osmium", BigInteger.valueOf(18), 110, Assets.Textures.Items.osmium),
    PLUTONIUM("plutonium", BigInteger.valueOf(25), 111),

    // Alloys
    BRONZE("bronze", BigInteger.valueOf(5), 201, Assets.Textures.Items.bronze,
           Recipe(4f, setOf(Ingredient(COPPER, 3f), Ingredient(TIN, 1f)))),
    STEEL("steel", BigInteger.valueOf(7), 202, Assets.Textures.Items.steel,
          Recipe(1f, setOf(Ingredient(IRON, 4f)))),
    ELECTRUM("electrum", BigInteger.valueOf(9), 203, Assets.Textures.Items.electrum,
             Recipe(2f, setOf(Ingredient(SILVER, 1f), Ingredient(GOLD, 1f))));

    /**
     * A recipe for creating [productAmount] of the parent item given the correct ratio of items
     * specified by [ingredients].
     *
     * Every element of ingredients has a unique item by virtue of [Ingredient]s being the same if
     * their items are the same.
     */
    class Recipe(val productAmount: Float, val ingredients: Set<Ingredient>)
    {
        /**
         * @return the ingredients needed to produce one unit of [product].item
         */
        val costOfOneProduct: Set<Ingredient> =
                ingredients.map { Ingredient(it.item, it.quantity / productAmount) }.toSet()
    }
}

interface ItemQuantity
{
    val item: Item
    val quantity: Number
    fun quantityToString(): String
}

/**
 * Helper class - essentially a [Pair]<[Item], Float> used to represent a specific amount of a certain item.
 */
open class ItemAmount(override val item: Item, override var quantity: Float) : ItemQuantity
{
    override fun quantityToString() = "${"%.2f".format(quantity)} kg"

    override fun equals(other: Any?): Boolean
    {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false

        other as ItemAmount

        if (item != other.item) return false
        if (quantity != other.quantity) return false

        return true
    }

    override fun hashCode(): Int
    {
        var result = item.hashCode()
        result = 31 * result + quantity.hashCode()
        return result
    }
}

/**
 * Similar to [ItemAmount], but for countable items
 */
data class ItemNumber(override val item: Item, override var quantity: Int) : ItemQuantity
{
    override fun quantityToString() = quantity.toString()
}

/**
 * Similar to ItemAmount, but equivalence only depends on [item] and not [quantity].
 */
class Ingredient(item: Item, quantity: Float) : ItemAmount(item, quantity)
{
    override fun quantityToString() = "${"%.0f".format(quantity)} kg"

    override fun equals(other: Any?): Boolean
    {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false

        other as Ingredient

        if (item != other.item) return false

        return true
    }

    override fun hashCode(): Int
    {
        return item.hashCode()
    }
}

// TODO: A more robust item drop system, like the one I made for my BetterBosses Terraria mod

/**
 * @param item the mineral being dropped
 * @param mean the average amount of item dropped. Can be less than 1, but not less than 0.
 * @param standardDeviation spread of the amount of item dropped. Must be greater than or equal to 0.
 */
data class ItemDrop(val item: Item, private val mean: Float = 1f, private val standardDeviation: Float = mean / 4f)
{
    private val random = Random()
    // We don't want to collect a negative amount of item, so get the absolute value of what we calculated
    // This is effectively the same as picking a random variable greater than 0 with probability defined by:
    // n(μ, σ) + n(-μ, σ)
    // where n is the normal distribution function
    fun getAmount() = Math.abs(random.nextGaussian() * standardDeviation + mean).toFloat()
}