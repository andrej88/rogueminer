package com.andrej.rogueminer.item

import com.andrej.rogueminer.item.FuelType.*
import com.andrej.rogueminer.util.Assets
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.math.MathUtils

// TODO: Storage bay

interface Part
{
    val name: String
    val type: String
    val ingredients: Set<Ingredient>
    val texture: Texture

    companion object
    {
        val all: Array<Part> = arrayOf(*Drill.values(),
                                       *Flywheel.values(),
                                       *Wiring.values(),
                                       *Engine.values(),
                                       *FuelTank.values())
    }
}

/**
 * @param miningLevel the higher the mining level, the higher the level of tiles you can mine
 * @param miningSpeed the higher the mining speed, the faster your drill. Might not make sense
 */
enum class Drill(val miningLevel: Int,
                 val miningSpeed: Float,
                 override val ingredients: Set<Ingredient> = emptySet(),
                 override val texture: Texture = Assets.Textures.Parts.debugTexture) : Part
{
    STOCK(1, 1f),
    COPPER(1, 1.3f, setOf(Ingredient(Item.COPPER, 20f)), Assets.Textures.Parts.Drill.copper),
    TIN(1, 1.2f, setOf(Ingredient(Item.TIN, 20f)), Assets.Textures.Parts.Drill.tin),
    SILVER(1, 5f),
    GOLD(1, 7f),
    LEAD(1, 2f),
    IRON(2, 1.2f),
    BRONZE(2, 1.5f, setOf(Ingredient(Item.BRONZE, 30f)), Assets.Textures.Parts.Drill.bronze),
    ALUMINIUM(2, 2.5f),
    STEEL(3, 2f, setOf(Ingredient(Item.STEEL, 35f)), Assets.Textures.Parts.Drill.steel),
    TITANIUM(4, 4f),
    OSMIUM(5, 5f, setOf(Ingredient(Item.OSMIUM, 50f)), Assets.Textures.Parts.Drill.osmium),
    TUNGSTEN(5, 4f),
    PLUTONIUM(2, 10f);

    override val type = "drill"
}

/**
 * Mostly true values in kg for 0.1 cubic meters of the metal
 *
 * @param mass greater mass -> drill stays at top speed longer -> better fuel efficiency
 */
enum class Flywheel(val mass: Float,
                    override val ingredients: Set<Ingredient> = emptySet(),
                    override val texture: Texture = Assets.Textures.Parts.debugTexture) : Part
{
    STOCK(500f),
    COPPER(896f),
    TIN(726f),
    SILVER(1050f),
    GOLD(1930f),
    LEAD(1130f),
    IRON(787f),
    BRONZE(820f),
    ALUMINIUM(270f),
    STEEL(790f),
    TITANIUM(451f),
    OSMIUM(2259f),
    TUNGSTEN(1930f),
    PLUTONIUM(1970f);

    override val type = "flywheel"
}

/**
 * @param conductivity the greater your conductivity, the lower your energy loss, the better your fuel efficiency.
 *
 * Only taken into consideration if engine is electrical.
 *
 * Units are Siemens/meter. True values for Copper through Plutonium
 */
enum class Wiring(val conductivity: Float,
                  override val ingredients: Set<Ingredient> = emptySet(),
                  override val texture: Texture = Assets.Textures.Parts.debugTexture) : Part
{
    STOCK(10f),
    COPPER(58.5f),
    TIN(8.7f),
    SILVER(62.1f),
    GOLD(44.2f),
    LEAD(4.7f),
    IRON(10.1f),
    BRONZE(7.4f),
    ALUMINIUM(36.9f),
    STEEL(5.9f),
    TITANIUM(2.4f),
    OSMIUM(12.31f),
    TUNGSTEN(18.9f),
    PLUTONIUM(0.685f),
    ELECTRUM(72f);

    override val type = "wiring"
}

// TODO: How to handle installing a fuel tank of a different type than the engine (or vice versa)

/**
 * @param power power output of the engine, greater values mean a faster Miner
 * @param efficiency proportional to power produced per unit fuel consumed
 * @param fuelType determines the tanks it's compatible with.
 */
enum class Engine(val power: Float, val efficiency: Float, val fuelType: FuelType,
                  override val ingredients: Set<Ingredient> = emptySet(),
                  override val texture: Texture = Assets.Textures.Parts.debugTexture) : Part
{
    CARBURETOR(3f, 1f, COMBUSTION),
    EXTREME_VOLTAGE(12f, 1f, ELECTRICAL);

    override val type = "engine"
}

/**
 * @param capacity how long you can go without refilling
 * @param types determines how to refill it and the engines it is compatible with (see [FuelType]
 * for detailed explanations of how each type relates to fuel tanks).
 */
enum class FuelTank(val capacity: Float, val types: Set<FuelType>,
                    override val ingredients: Set<Ingredient> = emptySet(),
                    override val texture: Texture = Assets.Textures.Parts.debugTexture) : Part
{
    BASIC(100f, COMBUSTION),
    RTG(Float.POSITIVE_INFINITY, ELECTRICAL);

    var amountOfFuel = 0f
        set(value)
        {
            field = MathUtils.clamp(value, 0f, capacity)
        }
    val percentFill: Float
        get() = amountOfFuel / capacity

    constructor(capacity: Float, type: FuelType) : this(capacity, setOf(type))

    override val type = "fuel tank"
}

/**
 * [COMBUSTION] - Takes regular combustible gasoline fuel. Basic engine type.
 *
 * [HYDROGEN] - Takes hydrogen and oxygen as fuel. Hydrogen has greater fuel density,so refuelling is not required as
 * as often as it is for the Combustion fuel type, but Hydrogen is more expensive. Engines
 * consuming hydrogen fuel produce water as a waste product. This means that an empty fuel tank
 * weighs more than a full one, but the "waste" water can then be sold. Due to the small size of
 * Hydrogen atoms, a hydrogen fuel tank "leaks" slowly over time.
 *
 * [ELECTRICAL] - Electrical "tanks" are basically batteries.
 */
enum class FuelType
{
    COMBUSTION, HYDROGEN, ELECTRICAL
}