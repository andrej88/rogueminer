package com.andrej.rogueminer.util

import com.badlogic.gdx.graphics.g2d.BitmapFont

val SCREEN_WIDTH = 800f
val SCREEN_HEIGHT = 600f

// Size of screen in UI tiles is 25 * 18.75
val uiTileSize = 32f
val pixelsPerUnit = 32f

val font = BitmapFont()


