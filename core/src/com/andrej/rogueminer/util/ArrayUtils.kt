package com.andrej.rogueminer.util

/**
 * Should NOT be constructed using the constructor, but instead be
 * constructed using Array2D.array2dOf() companion object functions.
 *
 * Constructing the two-dimensional Array requires the type parameter
 * to be reified. Because it cannot be reified in the constructor,
 * inlined factory functions with a reified type parameter must be
 * used instead.
 *
 * The actual constructor can't be made private because
 * then the inline factory functions wouldn't be able to call it.
 * However, it works if it is protected, even though it doesn't make
 * any sense to create a protected constructor in a final class.
 *
 * See http://stackoverflow.com/questions/28548647/create-generic-2d-array-in-kotlin
 */
class Array2D<T> protected constructor(val outerSize: Int, val innerSize: Int, private val array: Array<Array<T>>) : Iterable<T> {

    val size: Int
        get() = outerSize * innerSize

    fun contains(element: T) = array.any { it.contains(element) }

    fun containsAll(elements: Collection<T>) = elements.all { e -> array.any { i -> i.contains(e) } }

    override fun iterator(): Iterator<T> {
        return object : Iterator<T> {

            var currentOuter = 0
            var currentInner = 0

            override fun hasNext() = !(currentOuter == outerSize - 1 && currentInner == innerSize - 1)

            override fun next(): T {
                currentInner++
                if (currentInner >= innerSize) {
                    currentInner = 0
                    currentOuter++
                }
                return array[currentOuter][currentInner]
            }
        }
    }

    fun forEach(operation: (T) -> Unit) {
        array.forEach { it.forEach { operation.invoke(it) } }
    }

    fun forEachIndexed(operation: (outer: Int, inner: Int, element: T) -> Unit) {
        array.forEachIndexed { outer, p -> p.forEachIndexed { inner, t -> operation.invoke(outer, inner, t) } }
    }

    operator fun get(x: Int, y: Int): T {
        return array[x][y]
    }

    operator fun set(x: Int, y: Int, t: T) {
        array[x][y] = t
    }

    companion object {
        inline fun <reified T> array2dOf() = Array2D(0, 0, Array(0, { emptyArray<T>() }))

        inline fun <reified T> array2dOf(innerSize: Int, outerSize: Int) =
                Array2D(innerSize, outerSize, Array(innerSize, { arrayOfNulls<T>(outerSize) }))

        inline fun <reified T> array2dOf(innerSize: Int, outerSize: Int, initializer: (Int, Int) -> (T)): Array2D<T> {
            val array = Array(innerSize, {
                val x = it
                Array(outerSize, { initializer(x, it) })
            })
            return Array2D(innerSize, outerSize, array)
        }

        inline fun <reified T> array2dOf(innerSize: Int, outerSize: Int, initialValue: T): Array2D<T> {
            return array2dOf(innerSize, outerSize) { inner, outer -> initialValue }
        }
    }

}