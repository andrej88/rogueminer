package com.andrej.rogueminer.util

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.*
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import ktx.collections.gdxArrayOf

object Assets
{
    fun load()
    {
        UiSkins
        Textures
    }

    object UiSkins
    {
        val default = Skin(Gdx.files.internal("textures/ui/uiskin.json"))
    }

    object Textures
    {
        init
        {
            Ui
            Items
            World
        }

        val miner = Texture("textures/miner.png")

        object Ui
        {
            val translucentUiScreen = Texture("textures/ui/screen.png")
            val craftingButtonIcon = Texture("textures/ui/crafting_button.png")
            val sellIcon = Texture("textures/ui/sell.png")
            val currentItem = Texture("textures/ui/currentItem.png")
        }

        object Items
        {
            val debugTexture = Texture("textures/items/debug.png")

            val ice = Texture("textures/items/ice.png")

            val tin = Texture("textures/items/tin.png")
            val copper = Texture("textures/items/copper.png")
            val iron = Texture("textures/items/iron.png")
            val silver = Texture("textures/items/silver.png")
            val gold = Texture("textures/items/gold.png")
            val lead = Texture("textures/items/lead.png")
            val osmium = Texture("textures/items/osmium.png")

            val bronze = Texture("textures/items/bronze.png")
            val steel = Texture("textures/items/steel.png")
            val electrum = Texture("textures/items/electrum.png")

            init
            {
                debugTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
                ice.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
                tin.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
                copper.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
                iron.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
                silver.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
                gold.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
                lead.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
                osmium.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
                bronze.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
                steel.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
                electrum.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
            }
        }

        object Parts
        {
            val debugTexture = Texture("textures/items/debug.png")

            object Drill
            {
                val copper: Texture
                val tin: Texture
                val osmium: Texture

                val bronze: Texture
                val steel: Texture

                private val rgb: RGB
                private val hsv: HSV

                init
                {
                    rgb = RGB(0, 0, 0)
                    hsv = HSV(0f, 0f, 0f)

                    val shiny = Pixmap(Gdx.files.internal("textures/parts/drill/shiny.png"))
                    val dull = Pixmap(Gdx.files.internal("textures/parts/drill/dull.png"))

                    copper = Texture(colorize(Colors.copper, shiny))
                    tin = Texture(colorize(Colors.tin, shiny))
                    osmium = Texture("textures/parts/drill/osmium.png")

                    bronze = Texture(colorize(Colors.bronze, dull))
                    steel = Texture(colorize(Colors.steel, dull))
                }

                /**
                 * Sets the hue and saturation of each pixmap pixel to those of [color].
                 * Adjusts value according to following formula:
                 *
                 * If color.value > 0:
                 * `value_out = 1 + (1 - color.value) * (value_in - 1)`
                 *
                 * If color.value <= 0:
                 * value_out = (1 + color.value) * value_in
                 *
                 * @return a colorized copy of the input Pixmap
                 */
                private fun colorize(color: HSV, toCopyAndColorize: Pixmap): Pixmap
                {
                    val colorized = Pixmap(toCopyAndColorize.width, toCopyAndColorize.height, Pixmap.Format.RGBA8888)
                    colorized.drawPixmap(toCopyAndColorize, 0, 0)

                    for (i in 0..colorized.width - 1)
                    {
                        for (j in 0..colorized.height - 1)
                        {
                            val currentColor = colorized.getPixel(i, j)

                            rgb.red = currentColor ushr 24
                            rgb.green = (currentColor and 0xFF0000) ushr 16
                            rgb.blue = (currentColor and 0xFF00) ushr 8

                            rgb.toHSV(hsv)

                            hsv.hue = color.hue
                            hsv.saturation = color.saturation
                            hsv.value =
                                    if (color.value > 0f)
                                        1 + (1 - color.value) * (hsv.value - 1)
                                    else
                                        hsv.value + color.value * hsv.value

                            hsv.toRGB(rgb)

                            colorized.drawPixel(i, j, rgb.toRGBA8888(currentColor and 0xFF))
                        }
                    }

                    return colorized
                }
            }
        }

        object World
        {
            val foundry = Texture("textures/world/foundry.png")
            val generalStore = Texture("textures/world/general_store.png")

            val baseLeft = Texture("textures/world/building_base_left.png")
            val baseMiddle = Texture("textures/world/building_base_middle.png")
            val baseRight = Texture("textures/world/building_base_right.png")

            val dirt = gdxArrayOf(Texture("textures/world/dirt1.png"),
                                  Texture("textures/world/dirt2.png"),
                                  Texture("textures/world/dirt3.png"),
                                  Texture("textures/world/dirt4.png"),
                                  Texture("textures/world/dirt5.png"),
                                  Texture("textures/world/dirt6.png"),
                                  Texture("textures/world/dirt7.png"))

            val stone = gdxArrayOf(Texture("textures/world/stone1.png"),
                                   Texture("textures/world/stone2.png"),
                                   Texture("textures/world/stone3.png"),
                                   Texture("textures/world/stone4.png"))

            val lava = Texture("textures/world/lava.png")

            val ice = Texture("textures/world/ice.png")
            val tin = Texture("textures/world/tin.png")
            val copper = Texture("textures/world/copper.png")
            val iron = Texture("textures/world/iron.png")
            val silver = Texture("textures/world/silver.png")
            val gold = Texture("textures/world/gold.png")
            val lead = Texture("textures/world/lead.png")
            val osmium = Texture("textures/world/osmium.png")

            init
            {
                foundry.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
                generalStore.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
                baseLeft.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
                baseMiddle.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
                baseRight.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
                dirt.forEach { it.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear) }
                stone.forEach { it.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear) }
                lava.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
                ice.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
                tin.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
                copper.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
                iron.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
                silver.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
                gold.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
                lead.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
                osmium.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear)
            }
        }
    }

    object Colors
    {
        val copper = HSV(30f, 0.75f, 0f)
        val tin = HSV(62f, 0.18f, 0.2f)
        val osmium = HSV(227f, 0.64f, 0.4f)

        val bronze = HSV(30f, 0.6f, 0f)
        val steel = HSV(0f, 0f, -0.2f)
    }
}