package com.andrej.rogueminer.util

import java.math.BigInteger
import java.text.NumberFormat

fun formatMoney(amount: BigInteger): String = NumberFormat.getInstance().format(amount)