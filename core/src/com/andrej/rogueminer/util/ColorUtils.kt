package com.andrej.rogueminer.util

/**
 * @param hue 0..360
 * @param saturation 0..1
 * @param value 0..1 - can be negative if used to modify other colors, otherwise undefined behavior for negative values
 */
data class HSV(var hue: Float, var saturation: Float, var value: Float)
{
    fun toRGB(out: RGB = RGB(0, 0, 0)): RGB
    {
        out.red = 0
        out.green = 0
        out.blue = 0

        val chroma = value * saturation
        val chromaInt = (chroma * 255).toInt()
        val hue6 = hue / 60f
        val x = (chroma * (1 - Math.abs((hue6 % 2) - 1)) * 255f).toInt()

        when (hue6)
        {
            in 0f..1f ->
            {
                out.red = chromaInt
                out.green = x
            }

            in 1f..2f ->
            {
                out.red = x
                out.green = chromaInt
            }

            in 2f..3f ->
            {
                out.green = chromaInt
                out.blue = x
            }

            in 3f..4f ->
            {
                out.green = x
                out.blue = chromaInt
            }

            in 4f..5f ->
            {
                out.blue = chromaInt
                out.red = x
            }

            in 5f..6f ->
            {
                out.blue = x
                out.red = chromaInt
            }
        }

        out.red += (value * 255 - chromaInt).toInt()
        out.green += (value * 255 - chromaInt).toInt()
        out.blue += (value * 255 - chromaInt).toInt()

        return out
    }
}

/**
 * All values 0..255 inclusive. Undefined behavior when values outside of that range.
 */
data class RGB(var red: Int, var green: Int, var blue: Int)
{
    fun toHSV(out: HSV = HSV(0f, 0f, 0f)): HSV
    {
        val max = Math.max(red, Math.max(green, blue))
        val min = Math.min(red, Math.min(green, blue))
        val chroma = max - min
        val chromaFloat = chroma / 255f

        val redFloat = red / 255f
        val greenFloat = green / 255f
        val blueFloat = blue / 255f

        val hue6 =
                if (chroma == 0)
                {
                    out.hue / 60f // preserve the old hue
                }
                else if (max == red)
                {
                    ((greenFloat - blueFloat) / chromaFloat) % 6
                }
                else if (max == green)
                {
                    ((blueFloat - redFloat) / chromaFloat) + 2
                }
                else // max is blue
                {
                    ((redFloat - greenFloat) / chromaFloat) + 4
                }

        out.hue = 60f * hue6
        out.saturation = if (out.value == 0f) 0f else chromaFloat / out.value
        out.value = max / 255f

        return out
    }

    fun toRGBA8888(a: Int) = (red shl 24) or (green shl 16) or (blue shl 8) or a
}