package com.andrej.rogueminer.util

data class IntRect(val x: Int, val y: Int, val width: Int, val height: Int) {

    val left: Int
        get() = x

    val right: Int
        get() = x + width - 1

    val bottom: Int
        get() = y

    val top: Int
        get() = y + height

    init {
        if (width < 1 || height < 1) {
            throw Exception("Invalid IntRect: width and height must be greater than 0.")
        }
    }

    fun contains(x: Int, y: Int) = x in this.x..this.x + width - 1 && y in this.y..this.y + height - 1

    fun overlaps(other: IntRect) = (right in other.left..other.right ||
                                   left in other.left..other.right) &&
                                   (bottom in other.bottom..other.top ||
                                   top in other.bottom..other.top)
}