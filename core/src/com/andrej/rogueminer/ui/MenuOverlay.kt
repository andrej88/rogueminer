package com.andrej.rogueminer.ui

import com.andrej.rogueminer.util.*
import com.andrej.rogueminer.world.WorldScreen
import com.andrej.rogueminer.world.miner.Miner
import com.badlogic.gdx.Gdx

abstract class MenuOverlay(screen: WorldScreen, miner: Miner) : Overlay(screen, miner)
{

    override fun show() {
        super.show()
        Gdx.input.inputProcessor = stage
    }

    override fun render(delta: Float)
    {
        batch.begin()
        batch.draw(Assets.Textures.Ui.translucentUiScreen, 0f, 0f, SCREEN_WIDTH, SCREEN_HEIGHT)
        batch.end()
        super.render(delta)
    }

    override fun hide()
    {
        super.hide()
        Gdx.input.inputProcessor = miner.inputProcessor
    }

    open fun close()
    {
        screen.overlay = HUDOverlay(screen, miner)
    }
}