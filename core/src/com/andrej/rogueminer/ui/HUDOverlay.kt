package com.andrej.rogueminer.ui

import com.andrej.rogueminer.util.*
import com.andrej.rogueminer.world.WorldScreen
import com.andrej.rogueminer.world.miner.Miner
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.utils.Align
import ktx.scene2d.*

class HUDOverlay(screen: WorldScreen, miner: Miner) : Overlay(screen, miner)
{
    var fuelGauge = ProgressBar(0f, 1f, 0.01f, true, Assets.UiSkins.default)

    override fun initializeTable()
    {

        table {
            this@HUDOverlay.stage.addActor(this@table)
            setFillParent(true)
            top()
            pad(Value.percentHeight(0.05f))

            fuelGauge = slider(vertical = true) {
                it.height(96f)
                touchable = Touchable.disabled
            }

            add().width(32f)

            label("$ ${formatMoney(miner.money)}") {
                it.width(150f)
                setAlignment(Align.right)
                it.top()
            }

            add().expandX()

        }

    }
}