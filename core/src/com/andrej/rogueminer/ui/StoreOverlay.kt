package com.andrej.rogueminer.ui

import com.andrej.rogueminer.util.formatMoney
import com.andrej.rogueminer.world.WorldScreen
import com.andrej.rogueminer.world.miner.Miner
import com.badlogic.gdx.Input
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.utils.Align
import ktx.actors.*
import ktx.scene2d.*

class StoreOverlay(screen: WorldScreen, miner: Miner) : MenuOverlay(screen, miner)
{
    override fun initializeTable()
    {
        table {
            this@StoreOverlay.stage.addActor(this@table)
            setFillParent(true)
            top()
            pad(Value.percentWidth(0.1f))

            // Top Row

            add().width(24f)

            label("$ ${formatMoney(miner.money)}") {
                it.width(150f)
                setAlignment(Align.right)
            }

            add().expandX()

            label("Inventory") {
                it.width(256f)
                setAlignment(Align.center)
            }

            add().expandX()

            button {
                label("X")
                it.width(24f)
                it.height(24f)
                onClick { inputEvent, textButton -> close() }
                onKeyDown { event: InputEvent, widget: KButton, keycode: Int ->
                    when (keycode)
                    {
                        Input.Keys.ESCAPE, Input.Keys.X ->
                        {
                            close()
                        }
                        Input.Keys.D ->
                        {
                            toggleDebug()
                        }
                    }
                }
                stage.keyboardFocus = this@button
            }

            row()
            add()
            add()
            add()

            // Inventory table


            scrollPane {
                it.top()
                format()
                table {
                    miner.inventory.measurableSorted.forEach { ia ->
                        add(Image(ia.item.texture)).width(32f).height(32f)
                        label(ia.item.itemName.capitalize()) {
                            it.width(100f)
                        }
                        add()
                        label(ia.quantityToString()) {
                            it.right()
                        }
                        button {
                            it.width(80f)
                            label("$${ia.item.price} / kg")
                            onClick { inputEvent, button ->
                                if (ia.quantity >= 1f)
                                {
                                    ia.quantity -= 1f
                                    miner.money += ia.item.price
                                    rebuild()
                                }
                            }
                        }

                        row()
                    }
                }
            }
        }
    }
}