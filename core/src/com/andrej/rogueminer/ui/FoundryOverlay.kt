package com.andrej.rogueminer.ui

import com.andrej.rogueminer.item.*
import com.andrej.rogueminer.util.Assets
import com.andrej.rogueminer.world.WorldScreen
import com.andrej.rogueminer.world.miner.Miner
import com.badlogic.gdx.Input
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.utils.Align
import ktx.actors.*
import ktx.scene2d.*

class FoundryOverlay(screen: WorldScreen, miner: Miner) : MenuOverlay(screen, miner)
{

    override fun initializeTable()
    {

        table {
            this@FoundryOverlay.stage.addActor(this@table)
            setFillParent(true)
            top()
            pad(Value.percentWidth(0.1f))

            //// Top Row

            label("Inventory") {
                it.width(256f)
                setAlignment(Align.center)
            }

            add().width(32f)

            label("Crafting") {
                it.width(256f)
                setAlignment(Align.center)
            }

            add().expandX()

            button {
                label("X")
                it.width(24f)
                it.height(24f)
                onClick { inputEvent, textButton -> close() }
                onKeyDown { event: InputEvent, widget: KButton, keycode: Int ->
                    when (keycode)
                    {
                        Input.Keys.ESCAPE, Input.Keys.X ->
                        {
                            close()
                        }
                        Input.Keys.D ->
                        {
                            toggleDebug()
                        }
                    }
                }
                stage.keyboardFocus = this@button
            }

            row()

            //// Inventory/Crafting Tables

            // Inventory table
            scrollPane {
                it.top()
                format()
                table {
                    miner.inventory.forEach {
                        add(Image(it.item.texture)).width(32f).height(32f)
                        label(it.item.itemName.capitalize()) {
                            it.width(100f)
                        }
                        add()
                        label(it.quantityToString()) {
                            it.right()
                        }
                        row()
                    }
                }
            }

            add()

            // Crafting table
            scrollPane {
                it.top()
                format()
                table {
                    Item.values().forEach { item ->
                        // Add a sub-table of ingredients
                        if (item.recipe != null)
                        {
                            ingredientsRow(item.recipe.ingredients)

                            // add the button
                            craftButton {
                                inputEvent, button ->
                                miner.craftItem(item)
                                rebuild()
                            }

                            // add the product
                            val image = Image(item.texture)
                            image.addTextTooltip(item.itemName.capitalize())
                            add(image).width(32f).height(32f)
                            label("${"%.2f".format(item.recipe.productAmount)} kg")

                            row()
                        }
                    }

                    Part.all.forEach { part ->
                        if (part.ingredients.isNotEmpty())
                        {
                            ingredientsRow(part.ingredients)

                            val craft = craftButton { inputEvent, actor ->
                                miner.craftPart(part)
                                rebuild()
                            }

                            if (!miner.haveEnoughIngredients(part.ingredients))
                            {
                                craft.isDisabled = true
                            }

                            val image = Image(part.texture)
                            image.addTextTooltip("${part.name.toLowerCase().capitalize()} ${part.type}")
                            add(image).width(32f).height(32f)

                            if (miner.hasPart(part))
                            {
                                val checkMark = Image(Assets.Textures.Ui.currentItem)
                                checkMark.addTextTooltip("Equipped")
                                add(checkMark).width(32f).height(32f)
                            }

                            row()
                        }
                    }
                }
            }
        }
    }
}