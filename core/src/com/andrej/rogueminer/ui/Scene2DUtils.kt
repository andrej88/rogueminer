package com.andrej.rogueminer.ui

import com.andrej.rogueminer.item.Ingredient
import com.andrej.rogueminer.util.Assets
import com.badlogic.gdx.scenes.scene2d.*
import com.badlogic.gdx.scenes.scene2d.ui.Image
import ktx.actors.onClick
import ktx.scene2d.*

/**
 * Remove background and add an input listener to set scroll focus when hovering over the scroll pane.
 */
fun KScrollPane.format()
{
    style.background = null
    addListener(object : InputListener()
                {
                    override fun enter(event: InputEvent?, x: Float, y: Float, pointer: Int, fromActor: Actor?)
                    {
                        stage.scrollFocus = this@format
                    }
                })
}

fun KTableWidget.ingredientsRow(ingredients: Set<Ingredient>): KTableWidget
{
    return table {
        it.right()
        ingredients.forEach { ingredient ->
            val image = Image(ingredient.item.texture)
            image.addTextTooltip(ingredient.item.itemName.capitalize())
            add(image).width(32f).height(32f)
            label(ingredient.quantityToString()) {
                it.width(48f)
            }
        }
    }
}

inline fun KTableWidget.craftButton(crossinline listener: (event: InputEvent, actor: Actor) -> Unit) =
        button {
            add(Image(Assets.Textures.Ui.craftingButtonIcon))
            onClick(listener)
        }