package com.andrej.rogueminer.ui

import com.andrej.rogueminer.world.WorldScreen
import com.andrej.rogueminer.world.miner.Miner
import com.badlogic.gdx.Screen
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.scenes.scene2d.Stage

abstract class Overlay(protected val screen: WorldScreen,
                       protected val miner: Miner) : Screen
{

    protected val stage = Stage()
    protected val batch = SpriteBatch()
    private var debug = false

    abstract fun initializeTable()

    fun rebuild() {
        stage.clear()
        initializeTable()
    }

    override fun resize(width: Int, height: Int) {
        stage.viewport.update(width, height, true)
    }

    override fun render(delta: Float) {
        stage.act(delta)
        stage.draw()
    }

    override fun dispose() {
        stage.dispose()
    }

    /**
     * Called when WorldScreen.overlay gets set to an instance of this class.
     */
    override fun show() {
        // Table should be re-built every time it is requested
        rebuild()
    }

    /**
     * Called when WorldScreen.overlay gets set to an overlay that is not the current instance
     * overlay.
     * Default implementation is empty (super call not required).
     */
    override fun hide() = Unit

    final override fun pause() = Unit

    final override fun resume() = Unit

    fun toggleDebug()
    {
        debug = !debug
        stage.setDebugAll(debug)
    }
}