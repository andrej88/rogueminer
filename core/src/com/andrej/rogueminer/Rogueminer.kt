package com.andrej.rogueminer

import com.andrej.rogueminer.util.Assets
import com.andrej.rogueminer.world.*
import com.badlogic.gdx.*
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.scenes.scene2d.ui.TooltipManager
import ktx.scene2d.Scene2DSkin

object Rogueminer : Game() {

    override fun create() {

        Assets.load()

        Scene2DSkin.defaultSkin = Assets.UiSkins.default

        val ttm = TooltipManager.getInstance()
        ttm.initialTime = 0f
        ttm.subsequentTime = 0f
        ttm.resetTime = 0f
        ttm.animations = false
        Gdx.app.log("Rogueminer", "Initialized tooltips")

        screen = WorldScreen()
        screen.show()

        // Set linear filtering on all tile textures
        val linear = Texture.TextureFilter.Linear
        Tile.values().forEach {
            it.textures.forEach {
                it.setFilter(linear, linear)
            }
        }
    }

    override fun dispose() {
        super.dispose()
        screen.dispose()
    }
}
