package com.andrej.rogueminer.world.miner

import com.andrej.rogueminer.item.*
import com.andrej.rogueminer.ui.HUDOverlay
import com.andrej.rogueminer.util.*
import com.andrej.rogueminer.world.*
import com.badlogic.gdx.*
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.*
import com.badlogic.gdx.math.*
import java.math.BigInteger

/**
 * The player
 */
class Miner(private val world: World, private val screen: WorldScreen)
{

    // Technical stuff ////////////////////////////////////////////////////////
    val sprite: Sprite by lazy { Sprite(Assets.Textures.miner) }
    var worldX = 0
        set(value)
        {
            field = value
            visualX = worldX.toFloat()
        }
    var worldY = 0
        set(value)
        {
            field = value
            visualY = worldY.toFloat()
        }
    var visualX = 0.0f
    var visualY = 0.0f
    /** Time it takes to complete a rotation, in seconds. */
    private val rotationTime: Float
        get() = (1f / engine.power) * 2f
    private var state = State.STOPPED
    var facing = Direction.DOWN
        set(value)
        {
            field = value
            visualRotation = field.radians
        }
    private var rotatingTo: Direction = Direction.DOWN
    /** In radians */
    var visualRotation = 0.0f
        set(value)
        {
            field = value
            while (field >= MathUtils.PI2)
            {
                field -= MathUtils.PI2
            }
        }
    /** How far along in the rotation the Miner is */
    private var rotationProgress = 0.0f
    private val rotationInterpolation = Interpolation.fade
    val inputProcessor = MinerInputProcessor()


    // Gameplay-related stuff /////////////////////////////////////////////////

    var money: BigInteger = BigInteger("0")
    val inventory = Inventory()

    // TODO: Maybe make Miner start off with random drill, hull, etc. (reasonably low level of course)
    /** The hull's resistance to pressure and/or heat */
    private var hullStrength = 1
    var drill = Drill.OSMIUM
    var flywheel = Flywheel.STOCK
    var wiring = Wiring.STOCK
    var fuelTank = FuelTank.BASIC
        set(value)
        {
            field = value
            (screen.overlay as? HUDOverlay)?.fuelGauge?.setRange(0f, fuelTank.capacity)
        }
    var engine = Engine.EXTREME_VOLTAGE


    init
    {
        worldX = world.WIDTH / 2
        worldY = world.SURFACE
        sprite.texture.setFilter(Texture.TextureFilter.Linear,
                                 Texture.TextureFilter.Linear)
    }

    fun update(delta: Float)
    {
        when (state)
        {

            State.ROTATING ->
            {
                if (rotationProgress < 1.0f)
                {
                    visualRotation = MathUtils.lerpAngle(facing.radians,
                                                         rotatingTo.radians,
                                                         rotationInterpolation.apply(rotationProgress))
                    rotationProgress += delta / (rotationTime * if (facing.axis == rotatingTo.axis) 1.5f else 1f)
                }
                if (rotationProgress > 1.0f)
                {
                    facing = rotatingTo
                    rotationProgress = 0.0f
                    state = State.STOPPED
                    facing.startMovement(this, inputProcessor)
                }
            }

            State.FINISHING_MOVEMENT ->
            {

                // Finish movement

                if (!world.canMineTile(worldX + facing.unitVector.x.toInt(),
                                       worldY + facing.unitVector.y.toInt(),
                                       drill.miningLevel))
                {
                    state = State.STOPPED
                }
                else
                {

                    if (!facing.axis.isInNextTile(this))
                    {
                        moveOrDrill(delta, facing)
                    }

                    if (facing.axis.isInNextTile(this))
                    {
                        state = State.STOPPED
                        facing.axis.setWorldCoord(this,
                                                  if (facing.isPositive)
                                                      MathUtils.floor(facing.axis.getVisualCoord(this))
                                                  else
                                                      MathUtils.ceil(facing.axis.getVisualCoord(this)))
                        world.mineTile(this, worldX, worldY)
                    }
                }

                facing.startMovement(this, inputProcessor)
            }

            State.MOVING ->
            {
                if (world.canMineTile(worldX + facing.unitVector.x.toInt(),
                                      worldY + facing.unitVector.y.toInt(),
                                      drill.miningLevel))
                {

                    moveOrDrill(delta, facing)

                    if (facing.axis.isInNextTile(this))
                    {
                        facing.axis.setWorldCoord(this,
                                                  facing.axis.getWorldCoord(this) + facing.direction1D)
                        world.mineTile(this, worldX, worldY)
                    }

                }
                else
                {
                    state = State.STOPPED
                }
            }

            else ->
            {
            }
        }
    }

    private fun moveOrDrill(delta: Float, direction: Direction)
    {

        val tile = world.getTile(worldX + direction.unitVector.x.toInt(),
                                 worldY + direction.unitVector.y.toInt())
        if (tile != null)
        {
            direction.axis.setVisualCoord(this,
                                          direction.axis.getVisualCoord(this) +
                                          direction.direction1D * drill.miningSpeed * delta * tile.hardness)
        }
        else
        {
            direction.axis.setVisualCoord(this,
                                          direction.axis.getVisualCoord(this) + direction.direction1D * engine.power * delta)
        }
    }

    private fun startMovement(direction: Direction)
    {
        if (state == State.STOPPED)
        {
            if (facing == direction)
            {
                state = State.MOVING
            }
            else
            {
                state = State.ROTATING
                rotatingTo = direction
            }
        }
    }

    fun craftItem(item: Item)
    {
        val recipe = item.recipe ?: return

        // First ensure you have enough of every ingredient
        if (haveEnoughIngredients(recipe.ingredients))
        {
            // Then add/remove items from the inventory as necessary
            useIngredients(recipe.ingredients)

            // Add the product entry to the inventory if it doesn't already exist
            var productEntry = inventory.measurable.find { it.item == item }
            if (productEntry == null)
            {
                productEntry = ItemAmount(item, 0f)
                inventory.add(productEntry)
            }
            productEntry.quantity += recipe.productAmount
        }
    }

    fun craftPart(part: Part)
    {
        if (haveEnoughIngredients(part.ingredients))
        {
            useIngredients(part.ingredients)
            when (part)
            {
                is Drill -> drill = part
                else -> Gdx.app.log("Miner", "Crafting not yet implemented for $part!")
            }
        }
    }

    fun haveEnoughIngredients(ingredients: Set<Ingredient>): Boolean
    {
        val inventoryIngredients = inventory.measurable.filter { it.item in ingredients.map { it.item } }
        return inventoryIngredients.size == ingredients.size &&
               !inventoryIngredients.any { ii ->
                   ii.quantity < ingredients.find { it.item == ii.item }!!.quantity
               }
    }

    fun useIngredients(ingredients: Set<Ingredient>)
    {
        for (ingredient in ingredients)
        {
            (inventory.measurable.find { it.item == ingredient.item } ?:
             throw Exception("Can't find ${ingredient.item.itemName} in inventory for crafting!"))
                    .quantity -= ingredient.quantity
        }
    }

    fun hasPart(part: Part) =
            part == when (part)
            {
                is Drill -> drill
                is Flywheel -> flywheel
                is Wiring -> wiring
                is Engine -> engine
                is FuelTank -> fuelTank
                else -> false
            }

    fun draw(batch: Batch)
    {
        batch.draw(sprite,
                   visualX * pixelsPerUnit, visualY * pixelsPerUnit,
                   sprite.width * 0.5f, sprite.height * 0.5f,
                   sprite.width, sprite.height,
                   1f, 1f,
                   visualRotation * MathUtils.radiansToDegrees)
    }

    inner class MinerInputProcessor : InputProcessor
    {
        // TODO: Customizable controls
        var left = false
        var right = false
        var up = false

        var down = false

        override fun keyDown(keycode: Int): Boolean
        {
            when (keycode)
            {
                Input.Keys.LEFT ->
                {
                    left = true
                    startMovement(Direction.LEFT)
                }
                Input.Keys.RIGHT ->
                {
                    right = true
                    startMovement(Direction.RIGHT)
                }
                Input.Keys.UP ->
                {
                    up = true
                    startMovement(Direction.UP)
                }
                Input.Keys.DOWN ->
                {
                    down = true
                    startMovement(Direction.DOWN)
                }
                Input.Keys.X ->
                {
                    val building = world.buildings.find { it.rect.contains(worldX, worldY) }
                    if (building != null)
                    {
                        if (left) keyUp(Input.Keys.LEFT)
                        if (right) keyUp(Input.Keys.RIGHT)
                        if (up) keyUp(Input.Keys.UP)
                        if (down) keyUp(Input.Keys.DOWN)
                        screen.overlay = building.createOverlay(screen, this@Miner)
                    }
                }
                else -> return false
            }
            return true
        }

        override fun keyUp(keycode: Int): Boolean
        {
            when (keycode)
            {
                Input.Keys.LEFT ->
                {
                    left = false
                    if (facing == Direction.LEFT && state == State.MOVING)
                    {
                        state = State.FINISHING_MOVEMENT
                    }
                }
                Input.Keys.RIGHT ->
                {
                    right = false
                    if (facing == Direction.RIGHT && state == State.MOVING)
                    {
                        state = State.FINISHING_MOVEMENT
                    }
                }
                Input.Keys.UP ->
                {
                    up = false
                    if (facing == Direction.UP && state == State.MOVING)
                    {
                        state = State.FINISHING_MOVEMENT
                    }
                }
                Input.Keys.DOWN ->
                {
                    down = false
                    if (facing == Direction.DOWN && state == State.MOVING)
                    {
                        state = State.FINISHING_MOVEMENT
                    }
                }
                Input.Keys.D ->
                {
                    screen.overlay?.toggleDebug()
                }
                else -> return false
            }
            return true
        }

        override fun keyTyped(character: Char) = false

        override fun mouseMoved(screenX: Int, screenY: Int) = false

        override fun scrolled(amount: Int) = false

        override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int) = false

        override fun touchDragged(screenX: Int, screenY: Int, pointer: Int) = false

        override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int) = false

    }

    class Inventory : MutableSet<ItemQuantity>
    {
        val countable = mutableSetOf<ItemNumber>()

        val countableSorted: Iterable<ItemNumber>
            get() = countable.sortedBy { it.item.order }
        val measurable = mutableSetOf<ItemAmount>()

        val measurableSorted: Iterable<ItemAmount>
            get() = measurable.sortedBy { it.item.order }


        fun hasItem(item: Item) = countable.any { it.item == item } || measurable.any { it.item == item }

        // Implementations of MutableSet functions

        /*
        template for function(element):

            if (element is ItemNumber) {
                return countable.function(element)
            }
            else if (element is ItemAmount) {
                return measurable.function(element)
            }
            return false

        */

        /*
        template for function(elements):

            return countable.function(elements.filterIsInstance(ItemNumber::class.java)) &&
                   measurable.function(elements.filterIsInstance(ItemAmount::class.java))

        */

        override fun add(element: ItemQuantity): Boolean
        {
            if (element is ItemNumber)
            {
                return countable.add(element)
            }
            else if (element is ItemAmount)
            {
                return measurable.add(element)
            }
            return false
        }

        override fun addAll(elements: Collection<ItemQuantity>): Boolean
        {
            return countable.addAll(elements.filterIsInstance(ItemNumber::class.java)) &&
                   measurable.addAll(elements.filterIsInstance(ItemAmount::class.java))
        }

        override fun clear()
        {
            countable.clear()
            measurable.clear()
        }

        override fun remove(element: ItemQuantity): Boolean
        {
            if (element is ItemNumber)
            {
                return countable.remove(element)
            }
            else if (element is ItemAmount)
            {
                return measurable.remove(element)
            }
            return false
        }

        override fun removeAll(elements: Collection<ItemQuantity>): Boolean
        {
            return countable.removeAll(elements.filterIsInstance(ItemNumber::class.java)) &&
                   measurable.removeAll(elements.filterIsInstance(ItemAmount::class.java))
        }

        override fun retainAll(elements: Collection<ItemQuantity>): Boolean
        {
            return countable.retainAll(elements.filterIsInstance(ItemNumber::class.java)) &&
                   measurable.retainAll(elements.filterIsInstance(ItemAmount::class.java))
        }

        override val size: Int
            get() = countable.size + measurable.size

        override fun contains(element: ItemQuantity): Boolean
        {
            if (element is ItemNumber)
            {
                return countable.contains(element)
            }
            else if (element is ItemAmount)
            {
                return measurable.contains(element)
            }
            return false
        }

        override fun containsAll(elements: Collection<ItemQuantity>): Boolean
        {
            return countable.containsAll(elements.filterIsInstance(ItemNumber::class.java)) &&
                   measurable.containsAll(elements.filterIsInstance(ItemAmount::class.java))
        }

        override fun isEmpty(): Boolean
        {
            return countable.isEmpty() && measurable.isEmpty()
        }

        override fun iterator(): MutableIterator<ItemQuantity> =
                (countableSorted + measurableSorted).toMutableSet().iterator()

    }

    enum class Direction(val axis: Axis,
                         val radians: Float,
                         val unitVector: Vector2,
                         val direction1D: Int,
                         val startMovement: Miner.(MinerInputProcessor) -> Unit)
    {

        UP(Axis.Y, MathUtils.PI, Vector2(0f, 1f), 1, {
            if (it.up) this.startMovement(UP)
            else if (it.left) this.startMovement(LEFT)
            else if (it.right) this.startMovement(RIGHT)
            else if (it.down) this.startMovement(DOWN)
        }),

        RIGHT(Axis.X, MathUtils.PI / 2, Vector2(1f, 0f), 1, {
            if (it.right) this.startMovement(RIGHT)
            else if (it.left) this.startMovement(LEFT)
            else if (it.up) this.startMovement(UP)
            else if (it.down) this.startMovement(DOWN)
        }),

        LEFT(Axis.X, 3 * MathUtils.PI / 2, Vector2(-1f, 0f), -1, {
            if (it.left) this.startMovement(LEFT)
            else if (it.right) this.startMovement(RIGHT)
            else if (it.up) this.startMovement(UP)
            else if (it.down) this.startMovement(DOWN)
        }),

        DOWN(Axis.Y, 0f, Vector2(0f, -1f), -1, {
            if (it.down) this.startMovement(DOWN)
            else if (it.left) this.startMovement(LEFT)
            else if (it.right) this.startMovement(RIGHT)
            else if (it.up) this.startMovement(UP)
        });

        enum class Axis(val isInNextTile: (Miner) -> Boolean,
                        val getWorldCoord: (Miner) -> Int,
                        val getVisualCoord: (Miner) -> Float,
                        val setWorldCoord: Miner.(Int) -> Unit,
                        val setVisualCoord: Miner.(Float) -> Unit)
        {
            X({ it.facing.unitVector.x.toInt() * (it.visualX - it.worldX) - 1 >= 0 },
              { it.worldX }, { it.visualX }, { this.worldX = it }, { this.visualX = it }),
            Y({ it.facing.unitVector.y.toInt() * (it.visualY - it.worldY) - 1 >= 0 },
              { it.worldY }, { it.visualY }, { this.worldY = it }, { this.visualY = it });

        }

        val isPositive: Boolean
            get() = direction1D > 0

    }

    private enum class State
    {

        STOPPED, MOVING, FINISHING_MOVEMENT, ROTATING

    }
}
