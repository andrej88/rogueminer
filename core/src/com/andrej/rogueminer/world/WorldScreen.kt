package com.andrej.rogueminer.world

import com.andrej.rogueminer.ui.*
import com.andrej.rogueminer.util.*
import com.andrej.rogueminer.world.miner.Miner
import com.badlogic.gdx.*
import com.badlogic.gdx.graphics.*
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.utils.viewport.*
import com.badlogic.gdx.utils.Array as GdxArray

class WorldScreen : Screen
{

    val world = World()
    val player = Miner(world, this)
    val batch = SpriteBatch()
    val viewport: Viewport
    val camera: OrthographicCamera
    var overlay: Overlay? = null
        set(value)
        {
            if (field != value)
            {
                field?.hide()
                field?.dispose()
                field = value
                field?.show()
            }
        }

    init
    {
        world.generate()
        camera = OrthographicCamera(SCREEN_WIDTH, SCREEN_HEIGHT)
        viewport = FitViewport(SCREEN_WIDTH, SCREEN_HEIGHT, camera)
        overlay = HUDOverlay(this, player)
    }

    override fun render(delta: Float)
    {

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        player.update(delta)

        // Center camera
        camera.position.x = (player.visualX * pixelsPerUnit) + (player.sprite.width / 2f)
        camera.position.y = (player.visualY * pixelsPerUnit) + (player.sprite.height / 2f)

        // Clamp camera to world bounds
        camera.position.x = MathUtils.clamp(camera.position.x,
                                            camera.viewportWidth / 2,
                                            (world.WIDTH * pixelsPerUnit) - (camera.viewportWidth / 2))

        camera.position.y = MathUtils.clamp(camera.position.y,
                                            camera.viewportHeight / 2,
                                            (world.HEIGHT * pixelsPerUnit) - (camera.viewportHeight / 2))

        camera.update()

        batch.projectionMatrix = camera.combined

        batch.begin()

        world.drawBuildings(batch)
        player.draw(batch)
        world.drawTiles(batch, camera)

        batch.end()
        overlay?.render(delta)
    }

    override fun show()
    {
        Gdx.gl.glClearColor(0f, 0f, 0f, 1f)
        Gdx.input.inputProcessor = player.inputProcessor
    }

    override fun hide()
    {
    }

    override fun pause()
    {
    }

    override fun resume()
    {
    }

    override fun resize(width: Int, height: Int)
    {
        viewport.update(width, height, true)
        overlay?.resize(width, height)
    }

    override fun dispose()
    {
        batch.dispose()
        overlay?.dispose()
    }
}