package com.andrej.rogueminer.world

import com.andrej.rogueminer.item.*
import com.andrej.rogueminer.util.Assets
import com.badlogic.gdx.graphics.Texture
import ktx.collections.gdxArrayOf
import com.badlogic.gdx.utils.Array as GdxArray

/**
 * Weights arrays' sizes should be equal to that of the corresponding textures array. Sum of
 * elements in each should be 1. If it's less than 1, then the remaining weight is effectively given
 * to the first texture. If it's greater than 1, then the textures whose weights result in a total
 * greater than 1 won't be chosen.
 *
 * @param hardness Mining speed multiplier
 * @param miningLevel how strong of a drill is needed to break the tile in the first place
 * @param textures the textures to choose from, should not be empty
 * @param textureWeights probability that the texture that the respective index will be chosen
 * @param overlays the textures to overlay on top of the base texture. Can be empty.
 * @param overlayWeights probability that the overlay that the respective index will be chosen
 * @param drops the item dropped by the tile and its average. Can be empty.
 */
enum class Tile(val miningLevel: Int,
                val hardness: Float,
                val textures: GdxArray<Texture>,
                val textureWeights: GdxArray<Float>,
                val overlays: GdxArray<Texture> = gdxArrayOf(),
                val overlayWeights: GdxArray<Float> = gdxArrayOf(),
                val drops: GdxArray<ItemDrop> = gdxArrayOf()) {

    // Basic
    DIRT(1, 1.0f, Assets.Textures.World.dirt, gdxArrayOf(0.88f, 0.01f, 0.01f, 0.01f, 0.03f, 0.03f, 0.03f)),
    STONE(3, 0.5f, Assets.Textures.World.stone, gdxArrayOf(0.25f, 0.25f, 0.25f, 0.25f)),
    LAVA(Int.MAX_VALUE, 1.0f, Assets.Textures.World.lava),
    BASE_LEFT(Int.MAX_VALUE, 1.0f, Assets.Textures.World.dirt[0], Assets.Textures.World.baseLeft),
    BASE_MID(Int.MAX_VALUE, 1.0f, Assets.Textures.World.dirt[0], Assets.Textures.World.baseMiddle),
    BASE_RIGHT(Int.MAX_VALUE, 1.0f, Assets.Textures.World.dirt[0], Assets.Textures.World.baseRight),

    // Ore
    COPPER(1, 0.9f, Assets.Textures.World.dirt[0], Assets.Textures.World.copper, ItemDrop(Item.COPPER)),
    TIN(1, 0.95f, Assets.Textures.World.dirt[0], Assets.Textures.World.tin, ItemDrop(Item.TIN)),
    IRON(2, 0.8f, Assets.Textures.World.dirt[0], Assets.Textures.World.iron, ItemDrop(Item.IRON)),
    SILVER(1, 0.8f, Assets.Textures.World.dirt[0], Assets.Textures.World.silver, ItemDrop(Item.SILVER)),
    GOLD(1, 0.85f, Assets.Textures.World.dirt[0], Assets.Textures.World.gold, ItemDrop(Item.GOLD)),
    OSMIUM(4, 0.3f, Assets.Textures.World.stone[0], Assets.Textures.World.osmium, ItemDrop(Item.OSMIUM)),
    LEAD(1, 0.8f, Assets.Textures.World.dirt[0], Assets.Textures.World.lead, ItemDrop(Item.LEAD)),

    // Other resources
    ICE(1, 0.95f, Assets.Textures.World.dirt[0], Assets.Textures.World.ice, ItemDrop(Item.ICE));

    init {
        if (textures.size != textureWeights.size) {
            throw Exception("Tile ${this.name} has invalid textureWeights property")
        }
        if (overlayWeights.size != overlays.size) {
            throw Exception("Tile ${this.name} has invalid overlayWeights property")
        }
    }

    constructor(miningLevel: Int,
                miningTime: Float,
                texture: Texture,
                overlay: Texture? = null,
                itemDrop: ItemDrop? = null) :
            this(miningLevel,
                 miningTime,
                 gdxArrayOf(texture),
                 gdxArrayOf(1f),
                 if (overlay == null) gdxArrayOf() else gdxArrayOf(overlay),
                 if (overlay == null) gdxArrayOf() else gdxArrayOf(1f),
                 if (itemDrop == null) gdxArrayOf() else gdxArrayOf(itemDrop))
}

val ores = listOf(Tile.ICE,
                  Tile.COPPER,
                  Tile.TIN,
                  Tile.IRON,
                  Tile.SILVER,
                  Tile.GOLD,
                  Tile.LEAD,
                  Tile.OSMIUM)