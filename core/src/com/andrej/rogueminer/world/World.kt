package com.andrej.rogueminer.world

import com.andrej.rogueminer.item.ItemAmount
import com.andrej.rogueminer.ui.*
import com.andrej.rogueminer.util.*
import com.andrej.rogueminer.world.miner.Miner
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.Batch
import ktx.collections.isNotEmpty
import java.util.*

class World
{
    val HEIGHT = 200
    val WIDTH = 80
    val SURFACE = 180

    val tiles = Array2D.array2dOf<Tile?>(HEIGHT, WIDTH, initialValue = null)
    val tileTextureIndicesToUse = Array2D.array2dOf(HEIGHT, WIDTH, 0f)
    private val random = Random()

    val buildings: List<Building>

    init {

        buildings = listOf(
                Building("Foundry", IntRect(50, SURFACE, 8, 6),
                         Assets.Textures.World.foundry, ::FoundryOverlay),
                Building("General Store", IntRect(24, SURFACE, 8, 5),
                         Assets.Textures.World.generalStore, ::StoreOverlay)
        )

        for (i in 0..buildings.size - 2) {
            for (j in i + 1..buildings.size - 1) {
                if (buildings[i].rect.overlaps(buildings[j].rect)) {
                    throw Exception("Buildings overlap: ${buildings[i].name} and ${buildings[j].name}.")
                }
            }
        }

    }

    fun generate() {
        generateBase()
        populate()
        randomizeGraphics()
    }

    // Does basic world generation - cannot generate "clusters" of objects or anything like that.
    private fun generateBase() {
        println("Generating land...")

        tiles.forEachIndexed { row, column, tile ->
            tiles[row, column] = when (row) {
                in 0..10 -> Tile.LAVA
                in 10..50 -> Tile.STONE
                in 50..150 ->
                    when (random.nextFloat()) {
                        in 0.0f..(1.0f * ((row - 50) / 100.0f)) -> Tile.DIRT
                        else -> Tile.STONE
                    }
                in 150..SURFACE - 1 -> Tile.DIRT
                else -> null
            }
        }
    }


    private fun populate() {

        println("Populating world....")

        // Randomly distribute ore tiles throughout the world
        val iterations = 2000
        val oreCompatibleTiles = setOf(Tile.STONE, Tile.DIRT)
        for (i in 1..iterations) {
            val x = random.nextInt(WIDTH)
            val y = random.nextInt(HEIGHT)
            val ore = ores[random.nextInt(ores.size)]
            setTile(x, y, oreCompatibleTiles, ore)
        }

        // Set buildings' bases to be indestructible
        for (building in buildings) {
            setTile(building.rect.x, building.rect.y - 1, Tile.BASE_LEFT)
            for (i in building.rect.x + 1..building.rect.x + building.rect.width - 2) {
                setTile(i, building.rect.y - 1, Tile.BASE_MID)
            }
            setTile(building.rect.x + building.rect.width - 1, building.rect.y - 1, Tile.BASE_RIGHT)
        }
    }

    private fun randomizeGraphics() {
        tileTextureIndicesToUse.forEachIndexed {
            row, column, value ->
            tileTextureIndicesToUse[row, column] = random.nextFloat()
        }
    }

    /**
     * Sets tile at ([x],[y]) to null.
     *
     * Safe - does nothing if tiles is out of bounds.
     */
    private fun removeTile(x: Int, y: Int) {
        setTile(x, y, null)
    }

    /**
     * Mines the tile at ([x],[y]) if possible and gives the Miner the appropriate drops.
     *
     * Safe - does nothing if coordinates are out of bounds.
     */
    fun mineTile(miner: Miner, x: Int, y: Int)
    {

        if (isCoordinateInWorld(x, y)) {

            val tile = getTile(x, y)

            if (tile != null) {
                removeTile(x, y)

                for (drop in tile.drops) {

                    val inventoryEntry = miner.inventory.find { it.item == drop.item } as ItemAmount?
                    val itemAmount: ItemAmount

                    if (inventoryEntry == null) {
                        itemAmount = ItemAmount(drop.item, 0f)
                        miner.inventory.add(itemAmount)
                    }
                    else {
                        itemAmount = inventoryEntry
                    }

                    itemAmount.quantity += drop.getAmount()
                }
            }
        }
    }

    /**
     * @return true if the tile at ([x],[y]) can be mined by a drill of the given [strength]
     */
    fun canMineTile(x: Int, y: Int, strength: Int) =
            if (isCoordinateInWorld(x, y)) {
                val tile = tiles[y, x]
                tile == null || strength >= tile.miningLevel
            }
            else {
                false
            }

    /**
     * If the tile currently at ([x],[y]) is in the set [from], set the tile to [to].
     *
     * Safe - does nothing if tile is out of bounds.
     */
    fun setTile(x: Int, y: Int, from: Set<Tile?>, to: Tile?) {
        if (getTile(x, y) in from) {
            setTile(x, y, to)
        }
    }

    /**
     * Safe - returns null if an index is out of bounds.
     *
     * @return tile at ([x],[y])
     */
    fun getTile(x: Int, y: Int) =
            if (isCoordinateInWorld(x, y)) {
                tiles[y, x]
            }
            else {
                null
            }

    /**
     * Set tile at ([x],[y]) to [tile]
     *
     * Safe - does nothing if index is out of bounds.
     */
    fun setTile(x: Int, y: Int, tile: Tile?) {
        if (isCoordinateInWorld(x, y)) {
            tiles[y, x] = tile
        }
    }

    /**
     * @return true if ([x],[y]) is in the world's bounds.
     */
    fun isCoordinateInWorld(x: Int, y: Int) = x in 0..WIDTH - 1 && y in 0..HEIGHT - 1

    fun drawTiles(batch: Batch, camera: OrthographicCamera) {

        val leftMostTile = ((camera.position.x - 0.5f * SCREEN_WIDTH) / pixelsPerUnit).toInt()
        val rightMostTile = ((camera.position.x + 0.5f * SCREEN_WIDTH) / pixelsPerUnit).toInt()
        val bottomMostTile = ((camera.position.y - 0.5f * SCREEN_HEIGHT) / pixelsPerUnit).toInt()
        val topMostTile = ((camera.position.y + 0.5f * SCREEN_HEIGHT) / pixelsPerUnit).toInt()

        tiles.forEachIndexed { row, column, tile ->
            if (row in bottomMostTile..topMostTile &&
                column in leftMostTile..rightMostTile &&
                tile != null) {

                var texture = tile.textures[0]
                var target = tileTextureIndicesToUse[row, column]
                for ((i, weight) in tile.textureWeights.withIndex()) {
                    if (target < weight) {
                        texture = tile.textures[i]
                        break
                    }
                    target -= weight
                }
                batch.draw(texture, pixelsPerUnit * column.toFloat(), pixelsPerUnit * row.toFloat())

                if (tile.overlays.isNotEmpty()) {
                    texture = tile.overlays[0]
                    target = tileTextureIndicesToUse[row, column]
                    for ((i, weight) in tile.overlayWeights.withIndex()) {
                        if (target < weight) {
                            texture = tile.overlays[i]
                            break
                        }
                        target -= weight
                    }
                    batch.draw(texture,
                               pixelsPerUnit * column.toFloat(), pixelsPerUnit * row.toFloat(),
                               pixelsPerUnit, pixelsPerUnit)
                }
            }
        }
    }

    fun drawBuildings(batch: Batch) {
        buildings.forEach { it.draw(batch) }
    }
}
