package com.andrej.rogueminer.world

import com.andrej.rogueminer.ui.Overlay
import com.andrej.rogueminer.util.*
import com.andrej.rogueminer.world.miner.Miner
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch

class Building(val name: String,
               val rect: IntRect,
               val texture: Texture,
               val createOverlay: (WorldScreen, Miner) -> Overlay)
{

    fun draw(batch: Batch) {
        batch.draw(texture, pixelsPerUnit * rect.x, pixelsPerUnit * rect.y)
    }
}